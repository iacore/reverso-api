// Example code that deserializes and serializes the model.
// extern crate serde;
// #[macro_use]
// extern crate serde_derive;
// extern crate serde_json;
//
// use generated_module::refr;
//
// fn main() {
//     let json = r#"{"answer": 42}"#;
//     let model: refr = serde_json::from_str(&json).unwrap();
// }

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Refr {
    text_to_rephrase: String,

    processing_time_sec: f64,

    classification: String,

    number_of_words: i64,

    candidates: Vec<Candidate>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Candidate {
    candidate: String,

    classification: String,

    number_words: i64,

    number_words_different: i64,

    // seem to be between [0, 1]
    diversity_score: f64,

    correction: bool,
}
