// Example code that deserializes and serializes the model.
// extern crate serde;
// #[macro_use]
// extern crate serde_derive;
// extern crate serde_json;
//
// use generated_module::sample;
//
// fn main() {
//     let json = r#"{"answer": 42}"#;
//     let model: sample = serde_json::from_str(&json).unwrap();
// }

use serde::{Serialize, Deserialize};

use crate::lang::Lang;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Sample {
    id: String,

    from: Lang,

    to: Lang,

    input: Vec<String>,

    corrected_text: Option<serde_json::Value>,

    translation: Vec<String>,

    engines: Vec<String>,

    language_detection: LanguageDetection,

    context_results: ContextResults,

    truncated: bool,

    time_taken: i64,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ContextResults {
    rude_words: bool,

    colloquialisms: bool,

    risky_words: bool,

    results: Vec<Result>,

    total_context_calls_made: i64,

    time_taken_context: i64,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Result {
    translation: String,

    source_examples: Vec<String>,

    target_examples: Vec<String>,

    rude: bool,

    colloquial: bool,

    part_of_speech: Option<serde_json::Value>,

    frequency: i64,

    vowels: Option<serde_json::Value>,

    transliteration: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct LanguageDetection {
    detected_language: Lang,

    is_direction_changed: bool,

    original_direction: String,

    original_direction_context_matches: i64,

    changed_direction_context_matches: i64,

    time_taken: i64,
}


// Issue in line 27: The only value for this in the input is null, which means you probably need a more complete input sample.
// 27:     corrected_text: Option<serde_json::Value>,

// Issue in line 71: The only value for this in the input is null, which means you probably need a more complete input sample.
// 71:     part_of_speech: Option<serde_json::Value>,

// Issue in line 75: The only value for this in the input is null, which means you probably need a more complete input sample.
// 75:     vowels: Option<serde_json::Value>,

// Issue in line 77: The only value for this in the input is null, which means you probably need a more complete input sample.
// 77:     transliteration: Option<serde_json::Value>,
