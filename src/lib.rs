use color_eyre::eyre::Report;

mod lang;
mod rephrase_out;
mod tr_out;

pub use lang::Lang;

#[derive(serde::Serialize)]
#[allow(non_snake_case)]
pub struct TranslateOptions {
    pub sentenceSplitter: bool,
    pub origin: &'static str,
    pub contextResults: bool,
    pub languageDetection: bool,
}
impl Default for TranslateOptions {
    fn default() -> Self {
        Self {
            sentenceSplitter: true,
            origin: "translation.web",
            contextResults: true,
            languageDetection: true,
        }
    }
}

#[derive(serde::Serialize)]
pub struct TranslateInput {
    pub format: &'static str,
    pub from: Lang,
    pub to: Lang,
    pub input: String,
    pub options: TranslateOptions,
}

pub type TranslateOutput = tr_out::Sample;

pub fn translate(agent: &ureq::Agent, input: TranslateInput) -> Result<TranslateOutput, Report> {
    let result = agent
        .post("https://api.reverso.net/translate/v1/translation")
        .send_json(input)?;
    Ok(result.into_json()?)
}

pub struct RephraseInput {
    pub lang: Lang,
    pub candidates: u32,
    pub sentence: String,
}

pub type RephraseOutput = rephrase_out::Refr;

pub fn rephrase(agent: &ureq::Agent, input: RephraseInput) -> Result<RephraseOutput, Report> {
    let result = agent
        .get("https://rephraser-api.reverso.net/v1/rephrase")
        .query_pairs([
            ("language", format!("{}", &input.lang).as_str()),
            ("sentence", &input.sentence),
            ("candidates", &input.candidates.to_string()),
        ])
        .send_bytes(&[])?;
    Ok(result.into_json()?)
}
