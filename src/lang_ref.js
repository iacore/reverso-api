function doa(...args) {
    return [args[2], args[3]]
}

const a = [
    (
        doa(
            'Arabic',
            'العربية',
            'ar',
            'ara',
            1025,
            'Mehdi22k',
            'SDL',
            'ar-wikipedia'
        )
    ),
    (
        doa(
            'Bulgarian',
            'Български',
            'bg',
            'bul',
            1026,
            null,
            'SDL'
        )
    ),
    (doa('Catalan', 'Català', 'ca', 'cat', 1027, 'Laia22k')),
    (
        doa(
            'Chinese',
            '中文',
            'zh',
            'chi',
            1028,
            'Lulu22k',
            'SDL',
            'zh-pinyin'
        )
    ),
    (
        doa('Czech', 'Češky', 'cs', 'cze', 1029, 'Eliska22k', 'SDL')
    ),
    (doa('Danish', 'Dansk', 'da', 'dan', 1030, 'Mette22k', 'SDL')),
    (
        doa('German', 'Deutsch', 'de', 'ger', 1031, 'Andreas22k', 'NMT')
    ),
    (
        doa(
            'Greek',
            'Ελληνικά',
            'el',
            'gre',
            1032,
            'Dimitris22k',
            'SDL'
        )
    ),
    (
        doa('English', 'English', 'en', 'eng', 1033, 'Heather22k', 'NMT')
    ),
    (
        doa('Spanish', 'Español', 'es', 'spa', 1034, 'Antonio22k', 'NMT')
    ),
    (doa('Finnish', 'Suomi', 'fi', 'fin', 1035, 'Sanna22k')),
    (
        doa('French', 'Français', 'fr', 'fra', 1036, 'Alice22k', 'NMT')
    ),
    (
        doa(
            'Hebrew',
            'עברית',
            'he',
            'heb',
            1037,
            'he-IL-Asaf',
            'SDL'
        )
    ),
    (doa('Hungarian', 'Magyar', 'hu', 'hun', 1038, null, 'SDL')),
    (
        doa('Italian', 'Italiano', 'it', 'ita', 1040, 'Fabiana22k', 'NMT')
    ),
    (
        doa(
            'Japanese',
            '日本語',
            'ja',
            'jpn',
            1041,
            'Sakura22k',
            'SDL',
            'ja-latin'
        )
    ),
    (
        doa(
            'Korean',
            '조선말',
            'ko',
            'kor',
            1042,
            'Minji22k',
            'SDL'
        )
    ),
    (
        doa('Dutch', 'Nederlands', 'nl', 'dut', 1043, 'Sofie22k', 'SDL')
    ),
    (doa('Norwegian', 'Norsk', 'no', 'nor', 1044, 'Bente22k')),
    (doa('Polish', 'Polski', 'pl', 'pol', 1045, 'Monika22k', 'SDL')),
    (
        doa(
            'Romanian',
            'Română',
            'ro',
            'rum',
            1048,
            'ro-RO-Andrei',
            'SDL'
        )
    ),
    (
        doa(
            'Russian',
            'Русский',
            'ru',
            'rus',
            1049,
            'Alyona22k',
            'NMT',
            'ru-wikipedia'
        )
    ),
    (doa('Slovak', 'Slovák', 'sk', 'slo', 1051, null, 'SDL')),
    (doa('Swedish', 'Svenska', 'sv', 'swe', 1053, 'Emma22k', 'SDL')),
    (
        doa(
            'Thai',
            'ภาษาไทย',
            'th',
            'tha',
            1054,
            null,
            'SDL'
        )
    ),
    (
        doa('Turkish', 'Türkçe', 'tr', 'tur', 1055, 'Ipek22k', 'SDL')
    ),
    (
        doa(
            'Urdu',
            'اُردُو',
            'ur',
            'urd',
            1056
        )
    ),
    (
        doa(
            'Ukrainian',
            'Українська',
            'uk',
            'ukr',
            1058,
            null,
            'SDL'
        )
    ),
    (
        doa(
            'Persian',
            'فارسی',
            'fa',
            'per',
            1065,
            null,
            'SDL'
        )
    ),
    (
        doa(
            'Hindi',
            'हिन्दी',
            'hi',
            'hin',
            1081,
            null,
            'SDL'
        )
    ),
    (
        doa(
            'Hausa',
            'حَوْسَ',
            'ha',
            'hau',
            1128
        )
    ),
    (
        doa(
            'PortugueseBrasilian',
            'Português - Brasil',
            'pb',
            'ptb',
            1046,
            'Marcia22k',
            'SDL'
        )
    ),
    (
        doa('Portuguese', 'Português', 'pt', 'por', 2070, 'Celia22k', 'NMT')
    ),
    (
        doa(
            'Serbian',
            'Српски',
            'sr',
            'srp',
            3098,
            null,
            'SDL'
        )
    )
]
for (b of a) {
    console.log(JSON.stringify(b))
}