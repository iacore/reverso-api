use color_eyre::Report;
use reverso_api::*;

fn main() -> Result<(), Report> {
    color_eyre::install()?;

    let agent = ureq::builder()
        .user_agent("Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0")
        .middleware(
            |req: ureq::Request,
             next: ureq::MiddlewareNext|
             -> Result<ureq::Response, ureq::Error> {
                let req = req
                    // .set("Accept", "application/json, text/plain, */*")
                    // .set("Content-Type", "application/json")
                    .set("X-Reverso-Origin", "translation.web");
                next.handle(req)
            },
        )
        .build();

    // let input = TranslateInput {
    //         format: "text",
    //         from: Lang::de,
    //         // from: "ger",
    //         to: Lang::en,
    //         input: "Das kommt darauf an".into(),
    //         options: Default::default(),
    //     };
    // let output = translate(&agent, input)?;

    // _ = dbg!(output);

    let lang = Lang::de;
    let candidates = 6;
    let sentence = "Das kommt darauf an".to_owned();
    let input = RephraseInput {
        lang,
        candidates,
        sentence,
    };
    let out = rephrase(&agent, input)?;
    dbg!(out);

    Ok(())
}
