#[derive(serde::Serialize, serde::Deserialize, strum::Display, Debug)]
#[allow(non_camel_case_types)]
pub enum Lang {
    #[serde(alias = "ara")]
    ar,
    #[serde(alias = "bul")]
    bg,
    #[serde(alias = "cat")]
    ca,
    #[serde(alias = "chi")]
    zh,
    #[serde(alias = "cze")]
    cs,
    #[serde(alias = "dan")]
    da,
    #[serde(alias = "ger")]
    de,
    #[serde(alias = "gre")]
    el,
    #[serde(alias = "eng")]
    en,
    #[serde(alias = "spa")]
    es,
    #[serde(alias = "fin")]
    fi,
    #[serde(alias = "fra")]
    fr,
    #[serde(alias = "heb")]
    he,
    #[serde(alias = "hun")]
    hu,
    #[serde(alias = "ita")]
    it,
    #[serde(alias = "jpn")]
    ja,
    #[serde(alias = "kor")]
    ko,
    #[serde(alias = "dut")]
    nl,
    #[serde(alias = "nor")]
    no,
    #[serde(alias = "pol")]
    pl,
    #[serde(alias = "rum")]
    ro,
    #[serde(alias = "rus")]
    ru,
    #[serde(alias = "slo")]
    sk,
    #[serde(alias = "swe")]
    sv,
    #[serde(alias = "tha")]
    th,
    #[serde(alias = "tur")]
    tr,
    #[serde(alias = "urd")]
    ur,
    #[serde(alias = "ukr")]
    uk,
    #[serde(alias = "per")]
    fa,
    #[serde(alias = "hin")]
    hi,
    #[serde(alias = "hau")]
    ha,
    #[serde(alias = "ptb")]
    pb,
    #[serde(alias = "por")]
    pt,
    #[serde(alias = "srp")]
    sr,
    // can't hold data because https://github.com/serde-rs/serde/issues/912
    #[serde(other)]
    other,
}
